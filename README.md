# Projects using SolidJs

> A list of projects using SolidJs



- [playwright](https://github.com/microsoft/playwright) - Playwright is a framework for Web Testing and Automation. It allows testing Chromium, Firefox and WebKit with a single API.  - 43951  ⭐ - Apache License 2.0
  

- [supabase](https://github.com/supabase/supabase) - The open source Firebase alternative. Follow to stay updated about our public Beta. - 40323  ⭐ - Apache License 2.0
  

- [swiper](https://github.com/nolimits4web/swiper) - Most modern mobile touch slider with hardware accelerated transitions - 33028  ⭐ - MIT License
  

- [query](https://github.com/TanStack/query) - 🤖 Powerful asynchronous state management, server-state utilities and data fetching for TS/JS, React, Solid, Svelte and Vue. - 30693  ⭐ - MIT License
  

- [solid](https://github.com/solidjs/solid) - A declarative, efficient, and flexible JavaScript library for building user interfaces. - 23068  ⭐ - MIT License
  

- [astro](https://github.com/withastro/astro) - Build faster websites with Astro&#39;s next-gen island architecture 🏝✨ - 21747  ⭐ - Other
  

- [table](https://github.com/TanStack/table) - 🤖 Headless UI for building powerful tables &amp; datagrids for TS/JS -  React-Table, Vue-Table, Solid-Table, Svelte-Table - 19651  ⭐ - MIT License
  

- [learn-anything](https://github.com/learn-anything/learn-anything) - Organize world&#39;s knowledge, explore connections and curate learning paths - 14449  ⭐ - 
  

- [vercel](https://github.com/vercel/vercel) - Develop. Preview. Ship. - 9536  ⭐ - Apache License 2.0
  

- [sst](https://github.com/serverless-stack/sst) - 💥 SST makes it easy to build full-stack serverless apps. - 9404  ⭐ - MIT License
  

- [ag-grid](https://github.com/ag-grid/ag-grid) - The best JavaScript Data Table for building Enterprise Applications. Supports React / Angular / Vue / Plain JavaScript. - 9286  ⭐ - Other
  

- [unocss](https://github.com/unocss/unocss) - The instant on-demand atomic CSS engine. - 7358  ⭐ - MIT License
  

- [mitosis](https://github.com/BuilderIO/mitosis) - Write components once, run everywhere. Compiles to Vue, React, Solid, Angular, Svelte, and more.  - 7046  ⭐ - MIT License
  

- [vitest](https://github.com/vitest-dev/vitest) - A Vite-native test framework. It&#39;s fast! - 6835  ⭐ - MIT License
  

- [spectre.console](https://github.com/spectreconsole/spectre.console) - A .NET library that makes it easier to create beautiful console applications. - 6087  ⭐ - MIT License
  

- [getbem.github.io](https://github.com/getbem/getbem.github.io) - Get BEM to all people in simplest way - 5487  ⭐ - MIT License
  

- [js-framework-benchmark](https://github.com/krausest/js-framework-benchmark) - A comparison of the performance of a few popular javascript frameworks - 4909  ⭐ - Apache License 2.0
  

- [tsparticles](https://github.com/matteobruni/tsparticles) - tsParticles - Easily create highly customizable JavaScript particles effects, confetti explosions and fireworks animations and use them as animated backgrounds for your website. Ready to use components available for React.js, Vue.js (2.x and 3.x), Angular, Svelte, jQuery, Preact, Inferno, Solid, Riot and Web Components. - 4311  ⭐ - MIT License
  

- [builder](https://github.com/BuilderIO/builder) - Drag and drop Visual CMS for React, Vue, Angular, and more - 4016  ⭐ - MIT License
  

- [effector](https://github.com/effector/effector) - Business logic with ease ☄️ - 3954  ⭐ - MIT License
  

- [virtual](https://github.com/TanStack/virtual) - 🤖 Headless UI for Virtualizing Large Element Lists in JS/TS, React, Solid, Vue and Svelte - 3497  ⭐ - MIT License
  

- [nonebot2](https://github.com/nonebot/nonebot2) - 跨平台 Python 异步聊天机器人框架 / Asynchronous multi-platform chatbot framework written in Python - 2700  ⭐ - MIT License
  

- [lucide](https://github.com/lucide-icons/lucide) - Beautiful &amp; consistent icon toolkit made by the community. Open-source project and a fork of Feather Icons. - 2653  ⭐ - ISC License
  

- [uvu](https://github.com/lukeed/uvu) - uvu is an extremely fast and lightweight test runner for Node.js and the browser - 2602  ⭐ - MIT License
  

- [asciinema-player](https://github.com/asciinema/asciinema-player) - Web player for terminal session recordings - 2213  ⭐ - Apache License 2.0
  

- [asciinema-server](https://github.com/asciinema/asciinema-server) - Web app for hosting asciicasts - 1991  ⭐ - Apache License 2.0
  

- [unplugin-icons](https://github.com/antfu/unplugin-icons) - 🤹 Access thousands of icons as components on-demand universally. - 1955  ⭐ - MIT License
  

- [iconify](https://github.com/iconify/iconify) - Universal icon framework. One syntax for FontAwesome, Material Design Icons, DashIcons, Feather Icons, EmojiOne, Noto Emoji and many other open source icon sets (100&#43; icon sets, 100,000&#43; icons). SVG framework, React, Vue and Svelte components! - 1799  ⭐ - MIT License
  

- [spotube](https://github.com/KRTirtho/spotube) - A lightweight free Spotify 🎧 crossplatform-client 🖥📱 which handles playback manually, streams music using Youtube &amp; no Spotify premium account is needed 😱 - 1745  ⭐ - Other
  

- [zag](https://github.com/chakra-ui/zag) - Finite state machines for building accessible design systems and UI components. - 1575  ⭐ - MIT License
  

- [vite-plugin-ssr](https://github.com/brillout/vite-plugin-ssr) - Like Next.js / Nuxt but as do-one-thing-do-it-well Vite plugin. - 1552  ⭐ - MIT License
  

- [vite-plugin-pwa](https://github.com/vite-pwa/vite-plugin-pwa) - Zero-config PWA for Vite - 1512  ⭐ - MIT License
  

- [unplugin-auto-import](https://github.com/antfu/unplugin-auto-import) - Auto import APIs on-demand for Vite, Webpack and Rollup - 1484  ⭐ - MIT License
  

- [solid-start](https://github.com/solidjs/solid-start) - SolidStart, the Solid app framework - 1416  ⭐ - MIT License
  

- [previewjs](https://github.com/fwouts/previewjs) - Preview UI components in your IDE instantly - 1311  ⭐ - Other
  

- [piral](https://github.com/smapiot/piral) - Framework for next generation web apps using micro frontends. :rocket: - 1269  ⭐ - MIT License
  

- [typesafe-i18n](https://github.com/ivanhofer/typesafe-i18n) - A fully type-safe and lightweight internationalization library for all your TypeScript and JavaScript projects. - 1167  ⭐ - MIT License
  

- [linter](https://github.com/steelbrain/linter) - A Base Linter with Cow Powers http://steelbrain.me/linter/ - 1104  ⭐ - MIT License
  

- [vite-plugin-pages](https://github.com/hannoeru/vite-plugin-pages) - File system based route generator for ⚡️Vite - 1092  ⭐ - MIT License
  

- [liveblocks](https://github.com/liveblocks/liveblocks) - The all-in-one toolkit to build collaborative products like Figma, Notion, and more. - 1074  ⭐ - Apache License 2.0
  

- [custom-elements-everywhere](https://github.com/webcomponents/custom-elements-everywhere) - Custom Element &#43; Framework Interoperability Tests. - 999  ⭐ - Other
  

- [lume](https://github.com/lume/lume) - Create 3D web applications with HTML. Bring a new depth to your DOM! - 853  ⭐ - MIT License
  

- [iles](https://github.com/ElMassimo/iles) - 🏝 The joyful site generator - 824  ⭐ - MIT License
  

- [diamond-types](https://github.com/josephg/diamond-types) - The world&#39;s fastest CRDT. WIP. - 823  ⭐ - 
  

- [fastify-dx](https://github.com/fastify/fastify-dx) - A Full Stack Framework based on Fastify and Vite. Built for great developer experience without compromising on performance and stability. - 796  ⭐ - 
  

- [mayfly-go](https://github.com/may-fly/mayfly-go) - web版linux(终端 文件 脚本 进程)、数据库(mysql pgsql)、redis(单机 哨兵 集群)、mongo统一管理操作平台。 - 746  ⭐ - Apache License 2.0
  

- [motionone](https://github.com/motiondivision/motionone) -  - 720  ⭐ - MIT License
  

- [reatom](https://github.com/artalar/reatom) - Reatom - tiny and powerful reactive system with immutable nature - 707  ⭐ - MIT License
  

- [kretes](https://github.com/kreteshq/kretes) - A Programming Environment for TypeScript &amp; Deno  - 676  ⭐ - Other
  

- [felte](https://github.com/pablo-abc/felte) - An extensible form library for Svelte, Solid and React - 624  ⭐ - MIT License
  

- [hope-ui](https://github.com/hope-ui/hope-ui) - 🤞 The SolidJS component library you&#39;ve hoped for. - 622  ⭐ - MIT License
  

- [solid-router](https://github.com/solidjs/solid-router) - A universal router for Solid inspired by Ember and React Router - 565  ⭐ - MIT License
  

- [fireworks-js](https://github.com/crashmax-dev/fireworks-js) - 🎆 A simple fireworks library! Ready to use components available for React, Vue 3, Svelte, Angular, Preact, Solid, and Web Components. - 546  ⭐ - MIT License
  

- [mikado](https://github.com/nextapps-de/mikado) - Mikado is the webs fastest template library for building user interfaces. - 545  ⭐ - Apache License 2.0
  

- [neodrag](https://github.com/PuruVJ/neodrag) - One Draggable to rule them all 💍 - 542  ⭐ - MIT License
  

- [fastify-vite](https://github.com/fastify/fastify-vite) - This plugin lets you load a Vite client application and set it up for Server-Side Rendering (SSR) with Fastify. - 510  ⭐ - 
  

- [observables](https://github.com/maverick-js/observables) - A tiny (~850B minzipped) and extremely fast library for creating reactive observables via functions.  - 501  ⭐ - MIT License
  

- [solid-primitives](https://github.com/solidjs-community/solid-primitives) - A library of high-quality primitives that extend SolidJS reactivity. - 499  ⭐ - MIT License
  

- [bundlejs](https://github.com/okikio/bundlejs) - An online tool to quickly bundle &amp; minify your projects, while viewing the compressed gzip/brotli bundle size, all running locally on your browser. - 472  ⭐ - MIT License
  

- [ranger](https://github.com/TanStack/ranger) - ⚛️ Hooks for building range and multi-range sliders in React - 470  ⭐ - MIT License
  

- [effective-shell](https://github.com/dwmkerr/effective-shell) - Text, samples and website for my &#39;Effective Shell&#39; series. - 469  ⭐ - 
  

- [static-web-apps-cli](https://github.com/Azure/static-web-apps-cli) - Azure Static Web Apps CLI ✨ - 421  ⭐ - MIT License
  

- [locatorjs](https://github.com/infi-pc/locatorjs) -  - 393  ⭐ - 
  

- [password-generator](https://github.com/midudev/password-generator) - Best password generator agnostic to framework - 391  ⭐ - MIT License
  

- [goploy](https://github.com/zhenorzz/goploy) - Devops项目代码部署发布平台，Deploy, CI/CD, Terminal, Sftp, Server monitor, Crontab Manager. - 378  ⭐ - GNU General Public License v3.0
  

- [whyframe](https://github.com/bluwy/whyframe) - Develop components in isolation with just an iframe - 371  ⭐ - MIT License
  

- [typebeat](https://github.com/kofigumbs/typebeat) - Keyboard-controlled music sequencer, sampler, and synth - 369  ⭐ - GNU Affero General Public License v3.0
  

- [create-mf-app](https://github.com/jherr/create-mf-app) - CLI app to create Module Federation applications - 365  ⭐ - MIT License
  

- [10-javascript-frameworks](https://github.com/fireship-io/10-javascript-frameworks) - Comparison of 10 frontend JavaScript frameworks - 357  ⭐ - 
  

- [framework-benchmarks](https://github.com/BuilderIO/framework-benchmarks) - Test each framework for it&#39;s performance cost - 340  ⭐ - 
  

- [create-tauri-app](https://github.com/tauri-apps/create-tauri-app) - Rapidly scaffold out a new tauri app project. - 328  ⭐ - Apache License 2.0
  

- [vite-plugin-monkey](https://github.com/lisonge/vite-plugin-monkey) - vite plugin server and build *.user.js for Tampermonkey and Violentmonkey and Greasemonkey - 327  ⭐ - MIT License
  

- [solid-headless](https://github.com/lxsmnsyc/solid-headless) - Headless UI for SolidJS - 321  ⭐ - MIT License
  

- [nx-extensions](https://github.com/nxext/nx-extensions) - Nrwl Nx Extension for StencilJs, SvelteJS, SolidJS, ViteJS - 294  ⭐ - MIT License
  

- [papanasi](https://github.com/CKGrafico/papanasi) - 🥯Papanasi is the Frontend UI library to use cross Frameworks. A set of components to use in Angular, Preact, Qwik, React, Solid, Svelte, Vue and Web Components - 287  ⭐ - MIT License
  

- [octo](https://github.com/voracious/octo) - Build your knowledge base - 272  ⭐ - MIT License
  

- [vite-plugin-solid](https://github.com/solidjs/vite-plugin-solid) - A simple integration to run solid-js with vite - 257  ⭐ - 
  

- [solid-toast](https://github.com/ardeora/solid-toast) - Customizable Toast Notifications for SolidJS - 254  ⭐ - MIT License
  

- [ghostly](https://github.com/patriksvensson/ghostly) - Ghostly is a GitHub notification client for Windows 10/11 - 254  ⭐ - MIT License
  

- [templates](https://github.com/solidjs/templates) - Vite &#43; solid templates - 243  ⭐ - 
  

- [suid](https://github.com/swordev/suid) - A port of Material-UI (MUI) built with SolidJS. - 241  ⭐ - MIT License
  

- [rspc](https://github.com/oscartbeaumont/rspc) - A blazingly fast and easy to use TRPC-like server for Rust. - 232  ⭐ - MIT License
  

- [Slidy](https://github.com/Valexr/Slidy) - 📸 Sliding action script - 226  ⭐ - MIT License
  

- [coauthor](https://github.com/edemaine/coauthor) - Coauthor supercollaboration/discussion forum - 218  ⭐ - MIT License
  

- [bud](https://github.com/roots/bud) - ⚡️ Lightning fast webpack framework combining the best parts of Laravel Mix and Symfony Encore - 218  ⭐ - MIT License
  

- [squint](https://github.com/squint-cljs/squint) - ClojureScript syntax to JavaScript compiler - 213  ⭐ - 
  

- [solid-dnd](https://github.com/thisbeyond/solid-dnd) - A lightweight, performant, extensible drag and drop toolkit for Solid JS. - 210  ⭐ - MIT License
  

- [bauble](https://github.com/ianthehenry/bauble) - a playground for making 3D art with lisp and math - 205  ⭐ - 
  

- [viteshot](https://github.com/fwouts/viteshot) - Viteshot 📸 is a fast and simple component screenshot tool based on Vite. - 189  ⭐ - MIT License
  

- [cocreate](https://github.com/edemaine/cocreate) - Cocreate Shared Whiteboard/Drawing - 189  ⭐ - MIT License
  

- [vessel](https://github.com/vessel-js/vessel) - (Coming Soon) Framework-agnostic tool for building &amp; deploying fast apps/docs. Powered by Vite &amp; Vercel. - 181  ⭐ - MIT License
  

- [solid-aria](https://github.com/solidjs-community/solid-aria) - A library of high-quality primitives that help you build accessible user interfaces with SolidJS. - 180  ⭐ - Other
  

- [form-js](https://github.com/bpmn-io/form-js) - View and visually edit JSON-based forms. - 178  ⭐ - Other
  

- [micro-fes-beginner-to-expert](https://github.com/jherr/micro-fes-beginner-to-expert) - Code for &#34;Micro Frontends From Beginner to Expert&#34; - 177  ⭐ - 
  

- [gravity](https://github.com/digital-loukoum/gravity) - Next-generation backend framework with isomorphic capacities - 177  ⭐ - GNU General Public License v3.0
  

- [codeimage](https://github.com/riccardoperra/codeimage) - Create elegant screenshots of your source code. Built with SolidJS - 175  ⭐ - MIT License
  

- [cr-sqlite](https://github.com/aphrodite-sh/cr-sqlite) - Convergent, Replicated SQLite. Multi-writer and CRDT support for SQLite - 172  ⭐ - Apache License 2.0
  

- [noteworthy](https://github.com/benrbray/noteworthy) - Markdown editor with bidirectional links and excellent math support, powered by ProseMirror.  (In Development!) - 172  ⭐ - GNU Affero General Public License v3.0
  

- [solid-styled-components](https://github.com/solidjs/solid-styled-components) - A 1kb Styled Components library for Solid - 169  ⭐ - MIT License
  

- [trustix](https://github.com/nix-community/trustix) - Trustix: Distributed trust and reproducibility tracking for binary caches [maintainer=@adisbladis] - 164  ⭐ - 
  

- [ts-rest](https://github.com/ts-rest/ts-rest) - RPC-like client, contract, and server implementation for a pure REST API - 158  ⭐ - MIT License
  

- [kairo](https://github.com/3Shain/kairo) -  - 154  ⭐ - 
  

- [engineering-blog-samples](https://github.com/LoginRadius/engineering-blog-samples) - Code samples of LoginRadius Engineering Blog - 146  ⭐ - MIT License
  

- [solid-realworld](https://github.com/solidjs/solid-realworld) - A Solid Implementation of the Realworld Example App - 144  ⭐ - 
  

- [live_motion](https://github.com/benvp/live_motion) - High performance animation library for Phoenix LiveView - 142  ⭐ - MIT License
  

- [caldaria](https://github.com/lxsmnsyc/caldaria) - An SSR framework for SolidJS - 137  ⭐ - MIT License
  

- [solid-hackernews](https://github.com/solidjs/solid-hackernews) - Solid implementation of Hacker News - 134  ⭐ - 
  

- [tabloo](https://github.com/bluenote10/tabloo) - Minimalistic dashboard app for visualizing tabular data - 133  ⭐ - MIT License
  

- [solid-labels](https://github.com/lxsmnsyc/solid-labels) - Simple, reactive labels for SolidJS - 127  ⭐ - MIT License
  

- [macaron](https://github.com/macaron-css/macaron) - Typesafe CSS-in-JS with zero runtime, colocation, maximum safety and productivity - 126  ⭐ - 
  

- [create-vite-extra](https://github.com/bluwy/create-vite-extra) - Extra Vite templates - 123  ⭐ - MIT License
  

- [react-solid-state](https://github.com/solidjs/react-solid-state) - Auto tracking state management for modern React - 118  ⭐ - MIT License
  

- [usignal](https://github.com/WebReflection/usignal) - A blend of @preact/signals-core and solid-js basic reactivity API - 114  ⭐ - ISC License
  

- [react-compiler](https://github.com/trueadm/react-compiler) - An experimental React compiler for typed function component trees - 112  ⭐ - MIT License
  

- [obsidian-metatable](https://github.com/arnau/obsidian-metatable) - An Obsidian plugin to display the frontmatter section as a fully expanded table. - 109  ⭐ - MIT License
  

- [solid-site](https://github.com/solidjs/solid-site) - Code that powers the SolidJS.com platform. - 109  ⭐ - 
  

- [solid-use](https://github.com/lxsmnsyc/solid-use) - A collection of SolidJS utilities - 104  ⭐ - MIT License
  

- [fluent-emoji-maker](https://github.com/ddiu8081/fluent-emoji-maker) - 🤣 Generate your own Fluent Emojis! - 104  ⭐ - MIT License
  

- [solid-devtools](https://github.com/thetarnav/solid-devtools) - Library of developer tools, reactivity debugger &amp; Devtools Chrome extension for visualizing SolidJS reactivity graph - 104  ⭐ - MIT License
  

- [packemon](https://github.com/milesj/packemon) - 📦 Build and prepare packages for npm distribution using standardized configurations and practices. Gotta pack &#39;em all! - 101  ⭐ - MIT License
  

- [capri](https://github.com/capri-js/capri) - Build static sites with interactive islands - 100  ⭐ - MIT License
  


